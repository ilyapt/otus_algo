package heap_sort

import (
	"otus_algo/06_simple_sorts/test_lib"
	"testing"
)

func Test_Complex(t *testing.T) {
	test_lib.ComplexTestCase(t, 0, SelectionSort, HeapSort)
	test_lib.ComplexTestCase(t, 1, SelectionSort, HeapSort)
	test_lib.ComplexTestCase(t, 2, SelectionSort, HeapSort)
	test_lib.ComplexTestCase(t, 3, SelectionSort, HeapSort)
	test_lib.ComplexTestCase(t, 4, SelectionSort, HeapSort)
	test_lib.ComplexTestCase(t, 5, SelectionSort, HeapSort)
	test_lib.ComplexTestCase(t, 6, HeapSort)
	test_lib.ComplexTestCase(t, 7, HeapSort)
}

package heap_sort

func HeapSort(arr []uint) {
	for i := len(arr)/2 - 1; i >= 0; i-- {
		heapify(arr, i, len(arr))
	}
	for i := len(arr) - 1; i >= 0; i-- {
		heapify(arr, 0, i+1)
		arr[i], arr[0] = arr[0], arr[i]
	}
}

func heapify(arr []uint, root, size int) {
	l := 2*root + 1
	r := l + 1
	x := root
	if l < size && arr[l] > arr[x] {
		x = l
	}
	if r < size && arr[r] > arr[x] {
		x = r
	}
	if x == root {
		return
	}

	arr[root], arr[x] = arr[x], arr[root]
	heapify(arr, x, size)
}

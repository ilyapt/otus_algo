package heap_sort

func SelectionSort(arr []uint) {
	x := findMax(arr, len(arr))
	for j := len(arr) - 1; j > 0; j-- {
		arr[x], arr[j] = arr[j], arr[x]
		x = findMax(arr, j)
	}
}

func findMax(arr []uint, size int) int {
	x := 0
	for i := 1; i < size; i++ {
		if arr[i] > arr[x] {
			x = i
		}
	}
	return x
}

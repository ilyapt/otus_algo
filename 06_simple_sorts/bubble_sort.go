package simple_sorts

func BubbleSort(arr []uint) {
	l := len(arr)
	for i := 0; i < l; i++ {
		for j := 1; j < l-i; j++ {
			if arr[j-1] > arr[j] {
				arr[j-1], arr[j] = arr[j], arr[j-1]
			}
		}
	}
}

func BubbleSortOptimized(arr []uint) {
	l := len(arr)
	for i := 0; i < l; i++ {
		swapped := false
		for j := 1; j < l-i; j++ {
			if arr[j-1] > arr[j] {
				arr[j-1], arr[j] = arr[j], arr[j-1]
				swapped = true
			}
		}
		if !swapped {
			break
		}
	}
}

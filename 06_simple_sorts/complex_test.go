package simple_sorts

import (
	"otus_algo/06_simple_sorts/test_lib"
	"testing"
)

func Test_Complex(t *testing.T) {
	test_lib.ComplexTestCase(t, 0, BubbleSort, BubbleSortOptimized, InsertionSort, InsertionSortShift, InsertionSortBisec, ShellSort, ShellSortCiura, ShellSortSedgewick)
	test_lib.ComplexTestCase(t, 1, BubbleSort, BubbleSortOptimized, InsertionSort, InsertionSortShift, InsertionSortBisec, ShellSort, ShellSortCiura, ShellSortSedgewick)
	test_lib.ComplexTestCase(t, 2, BubbleSort, BubbleSortOptimized, InsertionSort, InsertionSortShift, InsertionSortBisec, ShellSort, ShellSortCiura, ShellSortSedgewick)
	test_lib.ComplexTestCase(t, 3, BubbleSort, BubbleSortOptimized, InsertionSort, InsertionSortShift, InsertionSortBisec, ShellSort, ShellSortCiura, ShellSortSedgewick)
	test_lib.ComplexTestCase(t, 4, BubbleSort, BubbleSortOptimized, InsertionSort, InsertionSortShift, InsertionSortBisec, ShellSort, ShellSortCiura, ShellSortSedgewick)
	test_lib.ComplexTestCase(t, 5, BubbleSort, BubbleSortOptimized, InsertionSort, InsertionSortShift, InsertionSortBisec, ShellSort, ShellSortCiura, ShellSortSedgewick)
	test_lib.ComplexTestCase(t, 6, ShellSort, ShellSortCiura, ShellSortSedgewick)
	test_lib.ComplexTestCase(t, 7, ShellSort, ShellSortSedgewick)
}

package test_lib

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"testing"
)

func ComplexTestCase(t *testing.T, level int, sortFn ...func([]uint)) {
	for _, v := range []string{"0.random", "1.digits", "2.sorted", "3.revers"} {
		l, unsorted, sorted := load(t, fmt.Sprintf("%s/test.%d", v, level))
		t.Run(fmt.Sprintf("%s/%d_(test.%d)", v, l, level), func(t *testing.T) {
			unsortedCopy := make([]uint, l)
			for _, fn := range sortFn {
				copy(unsortedCopy, unsorted)
				t.Run(fmt.Sprintf("%s", getFnName(fn)), func(t *testing.T) {
					fn(unsortedCopy)
					assert.EqualValues(t, sorted, unsortedCopy)
				})
			}
		})
	}
}

func TestSpecifiedCases(t *testing.T, level int, cases string, sortFn ...func([]uint)) {
	for _, v := range strings.Split(strings.TrimSpace(cases), ",") {
		l, unsorted, sorted := load(t, fmt.Sprintf("%s/test.%d", v, level))
		t.Run(fmt.Sprintf("%s/%d_(test.%d)", v, l, level), func(t *testing.T) {
			unsortedCopy := make([]uint, l)
			for _, fn := range sortFn {
				copy(unsortedCopy, unsorted)
				t.Run(fmt.Sprintf("%s", getFnName(fn)), func(t *testing.T) {
					fn(unsortedCopy)
					assert.EqualValues(t, sorted, unsortedCopy)
				})
			}
		})
	}
}

func load(t *testing.T, testCase string) (len int, unsorted []uint, sorted []uint) {
	dataIn, err := ioutil.ReadFile("./sorting-tests/" + testCase + ".in")
	require.NoError(t, err)
	lines := strings.Split(string(dataIn), "\n")
	len, err = strconv.Atoi(strings.TrimSpace(lines[0]))
	require.NoError(t, err)
	unsorted = make([]uint, len)
	for i, v := range strings.Split(strings.TrimSpace(lines[1]), " ") {
		n, err := strconv.ParseUint(v, 10, 32)
		require.NoError(t, err)
		unsorted[i] = uint(n)
	}
	dataOut, err := ioutil.ReadFile("./sorting-tests/" + testCase + ".out")
	require.NoError(t, err)
	sorted = make([]uint, len)
	for i, v := range strings.Split(strings.TrimSpace(string(dataOut)), " ") {
		n, err := strconv.ParseUint(v, 10, 32)
		require.NoError(t, err)
		sorted[i] = uint(n)
	}
	return len, unsorted, sorted
}

func getFnName(fn func([]uint)) string {
	return strings.Split(runtime.FuncForPC(reflect.ValueOf(fn).Pointer()).Name(), ".")[1]
}

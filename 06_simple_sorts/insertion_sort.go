package simple_sorts

func InsertionSort(arr []uint) {
	l := len(arr)
	for i := 1; i < l; i++ {
		for j := i; j > 0 && arr[j] < arr[j-1]; j-- {
			arr[j-1], arr[j] = arr[j], arr[j-1]
		}
	}
}

func InsertionSortShift(arr []uint) {
	l := len(arr)
	for i := 1; i < l; i++ {
		k := arr[i]
		var j int
		for j = i - 1; j >= 0 && k < arr[j]; j-- {
			arr[j+1] = arr[j]
		}
		arr[j+1] = k
	}
}

func InsertionSortBisec(arr []uint) {
	l := len(arr)
	for i := 1; i < l; i++ {
		m := arr[i]
		k := bisec(arr, 0, i, arr[i])
		for j := i - 1; j >= k; j-- {
			arr[j+1] = arr[j]
		}
		arr[k] = m
	}
}

func bisec(arr []uint, l, r int, value uint) int {
	mPos := l + (r-l)/2
	if r-l > 1 {
		if value < arr[mPos-1] {
			return bisec(arr, l, mPos-1, value)
		}
		if value > arr[mPos+1] {
			return bisec(arr, mPos+1, r, value)
		}
	}
	if value < arr[mPos] {
		return mPos
	}
	return mPos + 1
}

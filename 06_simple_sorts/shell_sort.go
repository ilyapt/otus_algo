package simple_sorts

import (
	"math"
)

func ShellSort(arr []uint) {
	l := len(arr)
	for s := l / 2; s > 0; s /= 2 {
		for i := s; i < l; i++ {
			for j := i - s; j >= 0 && arr[j] > arr[j+s]; j -= s {
				arr[j], arr[j+s] = arr[j+s], arr[j]
			}
		}
	}
}

func ShellSortCiura(arr []uint) {
	l := len(arr)
	gaps := []int{1750, 701, 301, 132, 57, 23, 10, 4, 1}
	for _, s := range gaps {
		for i := s; i < l; i++ {
			for j := i - s; j >= 0 && arr[j] > arr[j+s]; j -= s {
				arr[j], arr[j+s] = arr[j+s], arr[j]
			}
		}
	}
}

func ShellSortSedgewick(arr []uint) {
	l := len(arr)
	gaps := []int{1}
	i := 1
	for {
		x := pow(4, i) + 3*pow(2, i-1) + 1
		if x > l/2 {
			break
		}
		gaps = append(gaps, x)
		i++
	}

	for n := len(gaps) - 1; n >= 0; n-- {
		s := gaps[n]
		for i := s; i < l; i++ {
			for j := i - s; j >= 0 && arr[j] > arr[j+s]; j -= s {
				arr[j], arr[j+s] = arr[j+s], arr[j]
			}
		}
	}
}

func pow(k int, m int) int {
	return int(math.Pow(float64(k), float64(m)))
}

package quick_and_merge

import (
	"errors"
	"io"
	"os"
)

func ExternalSort(f, a, b *os.File) {
	for {
		split(f, a, b)

		offset, err := b.Seek(0, io.SeekCurrent) // this is getPos()
		if err != nil {
			panic(err)
		}
		if offset == 0 {
			break
		}

		f.Seek(0, io.SeekStart)
		f.Truncate(0)
		a.Seek(0, io.SeekStart)
		b.Seek(0, io.SeekStart)

		merge(f, a, b)

		f.Seek(0, io.SeekStart)
		a.Seek(0, io.SeekStart)
		b.Seek(0, io.SeekStart)
		a.Truncate(0)
		b.Truncate(0)
	}
}

func readFromFile(f *os.File, dat []byte) bool {
	_, err := f.Read(dat)
	if errors.Is(err, io.EOF) {
		return false
	} else if err != nil {
		panic(err)
	}
	return true
}

func split(f, a, b *os.File) {
	isA := true
	last := uint16(0)
	dat := make([]byte, 2)

	for {
		if !readFromFile(f, dat) {
			break
		}

		val := uint16(dat[0]) | uint16(dat[1])<<8
		if val < last {
			isA = !isA
		}

		if isA {
			a.Write(dat)
		} else {
			b.Write(dat)
		}
		last = val
	}
}

func merge(f, a, b *os.File) {
	datA := make([]byte, 2)
	datB := make([]byte, 2)
	readFromFile(a, datA)
	readFromFile(b, datB)

	for {
		if uint16(datA[0])|uint16(datA[1])<<8 <= uint16(datB[0])|uint16(datB[1])<<8 {
			f.Write(datA)
			if !readFromFile(a, datA) {
				f.Write(datB)
				break
			}
		} else {
			f.Write(datB)
			if !readFromFile(b, datB) {
				f.Write(datA)
				break
			}
		}
	}

	for {
		if !readFromFile(b, datA) {
			break
		}
		f.Write(datA)
	}

	for {
		if !readFromFile(b, datB) {
			break
		}
		f.Write(datB)
	}
}

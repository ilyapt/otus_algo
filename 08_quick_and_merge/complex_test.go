package quick_and_merge

import (
	"otus_algo/06_simple_sorts/test_lib"
	"testing"
)

func Test_Complex(t *testing.T) {
	test_lib.ComplexTestCase(t, 0, QuickSortV1, QuickSortV2, MergeSort)
	test_lib.ComplexTestCase(t, 1, QuickSortV1, QuickSortV2, MergeSort)
	test_lib.ComplexTestCase(t, 2, QuickSortV1, QuickSortV2, MergeSort)
	test_lib.ComplexTestCase(t, 3, QuickSortV1, QuickSortV2, MergeSort)
	test_lib.ComplexTestCase(t, 4, QuickSortV1, QuickSortV2, MergeSort)
	test_lib.ComplexTestCase(t, 5, QuickSortV1, QuickSortV2, MergeSort)
	test_lib.TestSpecifiedCases(t, 6, "0.random,1.digits,2.sorted", QuickSortV1, QuickSortV2, MergeSort)
	test_lib.TestSpecifiedCases(t, 6, "3.revers", MergeSort)
	test_lib.TestSpecifiedCases(t, 7, "0.random", QuickSortV1, QuickSortV2, MergeSort)
	test_lib.TestSpecifiedCases(t, 7, "1.digits,2.sorted,3.revers", MergeSort)
}

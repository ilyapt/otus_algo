package quick_and_merge

func QuickSortV1(arr []uint) {
	if len(arr) < 2 {
		return
	}
	m := partitionV1(arr)
	QuickSortV1(arr[:m])
	QuickSortV1(arr[m+1:])
}

func QuickSortV2(arr []uint) {
	if len(arr) < 2 {
		return
	}
	m := partitionV2(arr)
	QuickSortV2(arr[:m])
	QuickSortV2(arr[m+1:])
}

func partitionV1(arr []uint) int {
	l, r, last := 0, len(arr)-2, len(arr)-1
	pivot := arr[last]
	// сохраняю pivot нетронутым, чтобы потом его можно было поставить между левой и правой частью
	// (то есть на "свое место") и не использовать при рекурсивных сортировках
	// если такого не сделать, а добавить pivot к одной из частей, то возникнет проблема. если
	// в массиве есть повторяющиеся элементы. Например оно никогда не отсортирует массив [3, 3, 3]
	for {
		for l <= r && arr[l] <= pivot {
			l++
		}
		for l <= r && arr[r] > pivot {
			r--
		}
		if l > r {
			break
		}
		arr[l], arr[r] = arr[r], arr[l]
	}
	// ставим pivot на "свое место"
	arr[l], arr[last] = arr[last], arr[l]
	return l
}

func partitionV2(arr []uint) int {
	l, r := 0, len(arr)-1
	m := l - 1
	pivot := arr[r]
	for j := l; j <= r; j++ {
		if arr[j] <= pivot {
			m++
			arr[m], arr[j] = arr[j], arr[m]
		}
	}
	return m
}

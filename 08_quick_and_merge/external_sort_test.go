package quick_and_merge

import (
	"github.com/stretchr/testify/require"
	"io"
	"os"
	"testing"
)

const N = 10000

func Test_ExternalSort(t *testing.T) {
	require.NoError(t, GenerateRandomFile(N, "./random.dat"))

	f, err := os.OpenFile("./random.dat", os.O_RDWR, 0)
	require.NoError(t, err)
	defer f.Close()

	a, err := os.Create("./tmp_a.dat")
	require.NoError(t, err)
	defer a.Close()

	b, err := os.Create("./tmp_b.dat")
	require.NoError(t, err)
	defer b.Close()

	ExternalSort(f, a, b)

	t.Run("verify", func(t *testing.T) {
		f.Seek(0, io.SeekStart)
		dat := make([]byte, 2)
		last := uint16(0)
		for {
			if !readFromFile(f, dat) {
				break
			}

			val := uint16(dat[0]) | uint16(dat[1])<<8
			if val < last {
				t.Fatal("incorrect order")
			}
			last = val
		}
	})
}

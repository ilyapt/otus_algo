package quick_and_merge

func MergeSort(arr []uint) {
	size := len(arr)
	if size < 2 {
		return
	}
	m := size / 2
	MergeSort(arr[:m])
	MergeSort(arr[m:])

	tmp := make([]uint, size)
	i, l, r := 0, 0, m
	for l < m && r < size {
		if arr[l] < arr[r] {
			tmp[i] = arr[l]
			l++
		} else {
			tmp[i] = arr[r]
			r++
		}
		i++
	}
	for l < m {
		tmp[i] = arr[l]
		l++
		i++
	}
	for r < size {
		tmp[i] = arr[r]
		r++
		i++
	}
	for i, v := range tmp {
		arr[i] = v
	}
}

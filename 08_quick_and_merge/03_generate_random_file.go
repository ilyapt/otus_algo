package quick_and_merge

import (
	"math/rand"
	"os"
	"time"
)

func GenerateRandomFile(N uint, file string) error {
	rand.Seed(time.Now().UnixMicro())

	f, err := os.Create(file)
	if err != nil {
		return err
	}
	defer f.Close()

	data := make([]byte, 2)
	for i := uint(0); i < N; i++ {
		rand.Read(data)
		if _, err := f.Write(data); err != nil {
			return err
		}
	}
	return nil
}

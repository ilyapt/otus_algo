package bit_arithmetic

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"strconv"
	"strings"
	"testing"
)

func Test_King(t *testing.T) {
	for i := 0; i < 10; i++ {
		t.Run(fmt.Sprintf("King %d", i), func(t *testing.T) {
			pos, cnt, expectedMask := loadTestArtifacts(t, "1.king", i)
			mask := kingMoves(pos)
			assert.Equal(t, expectedMask, mask)
			assert.Equal(t, cnt, countingSimple(mask))
			assert.Equal(t, cnt, countingSubOneAnd(mask))
			assert.Equal(t, cnt, countingWithCache(mask))
		})
	}
}

func Test_Knight(t *testing.T) {
	for i := 0; i < 10; i++ {
		t.Run(fmt.Sprintf("Knight %d", i), func(t *testing.T) {
			pos, cnt, expectedMask := loadTestArtifacts(t, "2.knight", i)
			mask := knightMoves(pos)
			assert.Equal(t, expectedMask, mask)
			assert.Equal(t, cnt, countingSimple(mask))
			assert.Equal(t, cnt, countingSubOneAnd(mask))
			assert.Equal(t, cnt, countingWithCache(mask))
		})
	}
}

func Test_Rook(t *testing.T) {
	for i := 0; i < 10; i++ {
		t.Run(fmt.Sprintf("Rook %d", i), func(t *testing.T) {
			pos, cnt, expectedMask := loadTestArtifacts(t, "3.rook", i)
			mask := rookMoves(pos)
			assert.Equal(t, expectedMask, mask)
			assert.Equal(t, cnt, countingSimple(mask))
			assert.Equal(t, cnt, countingSubOneAnd(mask))
			assert.Equal(t, cnt, countingWithCache(mask))
		})
	}
}

func Test_Bishop(t *testing.T) {
	for i := 0; i < 10; i++ {
		t.Run(fmt.Sprintf("Bishop %d", i), func(t *testing.T) {
			pos, cnt, expectedMask := loadTestArtifacts(t, "4.bishop", i)
			mask := bishopMoves(pos)
			assert.Equal(t, expectedMask, mask)
			assert.Equal(t, cnt, countingSimple(mask))
			assert.Equal(t, cnt, countingSubOneAnd(mask))
			assert.Equal(t, cnt, countingWithCache(mask))
		})
	}
}

func Test_Queen(t *testing.T) {
	for i := 0; i < 10; i++ {
		t.Run(fmt.Sprintf("Queen %d", i), func(t *testing.T) {
			pos, cnt, expectedMask := loadTestArtifacts(t, "5.queen", i)
			mask := queenMoves(pos)
			assert.Equal(t, expectedMask, mask)
			assert.Equal(t, cnt, countingSimple(mask))
			assert.Equal(t, cnt, countingSubOneAnd(mask))
			assert.Equal(t, cnt, countingWithCache(mask))
		})
	}
}

func loadTestArtifacts(t *testing.T, path string, num int) (uint, uint, uint64) {
	in, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.in", path, num))
	require.NoError(t, err)
	pos, err := strconv.Atoi(strings.TrimSpace(string(in)))
	require.NoError(t, err)
	out, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.out", path, num))
	require.NoError(t, err)
	lines := strings.Split(strings.TrimSpace(string(out)), "\n")
	count, err := strconv.Atoi(strings.TrimSpace(lines[0]))
	require.NoError(t, err)
	mask, err := strconv.ParseUint(strings.TrimSpace(lines[1]), 10, 64)
	return uint(pos), uint(count), mask
}

package bit_arithmetic

var countConstants [256]uint

func init() {
	for i := 0; i < 256; i++ {
		countConstants[i] = countingSubOneAnd(uint64(i))
	}
}

func countingSimple(mask uint64) (cnt uint) {
	for mask > 0 {
		if mask&1 == 1 {
			cnt++
		}
		mask >>= 1
	}
	return cnt
}

func countingSubOneAnd(mask uint64) (cnt uint) {
	for mask > 0 {
		mask &= mask - 1
		cnt++
	}
	return cnt
}

func countingWithCache(mask uint64) (cnt uint) {
	for mask > 0 {
		cnt += countConstants[mask&255]
		mask >>= 8
	}
	return cnt
}

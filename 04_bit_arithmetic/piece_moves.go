package bit_arithmetic

func kingMoves(pos uint) uint64 {
	var K uint64 = 1 << pos
	Ka := K & 0xfefefefefefefefe
	Kh := K & 0x7f7f7f7f7f7f7f7f

	mask := (Ka << 7) | (K << 8) | (Kh << 9) |
		(Ka >> 1) | (Kh << 1) |
		(Ka >> 9) | (K >> 8) | (Kh >> 7)

	return mask
}

func knightMoves(pos uint) uint64 {
	var K uint64 = 1 << pos
	Ka := K & 0xfefefefefefefefe
	Kab := K & 0xfcfcfcfcfcfcfcfc
	Kgh := K & 0x3f3f3f3f3f3f3f3f
	Kh := K & 0x7f7f7f7f7f7f7f7f

	mask := (Ka << 15) | (Kab << 6) | (Kh << 17) | (Kgh << 10) |
		(Ka >> 17) | (Kab >> 10) | (Kh >> 15) | (Kgh >> 6)
	return mask
}

func rookMoves(pos uint) uint64 {
	var K uint64 = 1 << pos
	var col uint64 = 0x101010101010101
	var row uint64 = 0xff
	x := pos % 8
	y := pos / 8
	return K ^ ((col << (1 * x)) | (row << (8 * y)))
}

func bishopMoves(pos uint) uint64 {
	var K uint64 = 1 << pos
	var p1 uint64 = 0xfefefefefefefefe
	var p2 uint64 = 0x7f7f7f7f7f7f7f7f
	step1 := 7
	step2 := 9

	var mask uint64
	for i := 1; i < 8; i++ {
		mask |= ((K & p1) >> (step2 * i)) | ((K & p2) << (step2 * i)) |
			((K & p1) << (step1 * i)) | ((K & p2) >> (step1 * i))
		p1 &= p1 << 1
		p2 &= p2 >> 1
	}
	return mask
}

func queenMoves(pos uint) uint64 {
	return rookMoves(pos) | bishopMoves(pos)
}

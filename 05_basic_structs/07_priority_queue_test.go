package basic_structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_PriorityQueue(t *testing.T) {
	q := NewPriorityQueue[int]()
	q.enqueue(10, 100)
	q.enqueue(11, 101)
	q.enqueue(10, 102)
	q.enqueue(10, 103)
	q.enqueue(12, 104)
	q.enqueue(10, 105)
	q.enqueue(12, 106)
	q.enqueue(10, 107)
	q.enqueue(11, 108)
	assert.EqualValues(t, 104, q.dequeue())
	assert.EqualValues(t, 106, q.dequeue())
	assert.EqualValues(t, 101, q.dequeue())
	assert.EqualValues(t, 108, q.dequeue())
	assert.EqualValues(t, 100, q.dequeue())
	assert.EqualValues(t, 102, q.dequeue())
	assert.EqualValues(t, 103, q.dequeue())
	assert.EqualValues(t, 105, q.dequeue())
	assert.EqualValues(t, 107, q.dequeue())
	assert.EqualValues(t, 0, q.dequeue())
}

package basic_structs

import (
	"sort"
)

type queueListItem[T any] struct {
	value T
	next  *queueListItem[T]
}

type queueLinkedList[T any] struct {
	head *queueListItem[T]
	tail *queueListItem[T]
}

func (l *queueLinkedList[T]) Put(item T) {
	e := &queueListItem[T]{value: item}
	if l.head == nil {
		l.head = e
	}
	if l.tail != nil {
		l.tail.next = e
	}
	l.tail = e
}

func (l *queueLinkedList[T]) Get() (result T, ok bool) {
	e := l.head
	if e == nil {
		return
	}
	if l.head != nil {
		l.head = l.head.next
	}
	return e.value, true
}

type priorityQueue[T any] struct {
	list []uint
	dict map[uint]*queueLinkedList[T]
}

func NewPriorityQueue[T any]() *priorityQueue[T] {
	return &priorityQueue[T]{
		dict: make(map[uint]*queueLinkedList[T]),
	}
}

func (q *priorityQueue[T]) enqueue(priority uint, item T) {
	if _, ok := q.dict[priority]; !ok {
		q.dict[priority] = &queueLinkedList[T]{}
		q.list = append(q.list, priority)
		sort.Slice(q.list, func(i, j int) bool {
			return q.list[i] > q.list[j]
		})
	}
	q.dict[priority].Put(item)
}

func (q *priorityQueue[T]) dequeue() (result T) {
	for _, p := range q.list {
		if item, ok := q.dict[p].Get(); ok {
			return item
		}
	}
	return
}

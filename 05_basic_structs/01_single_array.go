package basic_structs

type singleArray[T any] struct {
	arr []T
}

func NewSingleArray[T any]() *singleArray[T] {
	return &singleArray[T]{arr: make([]T, 0)}
}

func (arr *singleArray[T]) Size() uint {
	return uint(len(arr.arr))
}

func (arr *singleArray[T]) IsEmpty() bool {
	return arr.Size() == 0
}

func (arr *singleArray[T]) Get(index uint) T {
	return arr.arr[index]
}

func (arr *singleArray[T]) Set(item T, index uint) {
	arr.arr[index] = item
}

func (arr *singleArray[T]) Put(item T) {
	arr.resize()
	arr.arr[arr.Size()-1] = item
}

func (arr *singleArray[T]) resize() {
	newArr := make([]T, arr.Size()+1)
	for i, v := range arr.arr {
		newArr[i] = v
	}
	arr.arr = newArr
}

func (arr *singleArray[T]) Add(item T, index uint) {
	newArr := make([]T, arr.Size()+1)
	for i := uint(0); i < index; i++ {
		newArr[i] = arr.arr[i]
	}
	for i := uint(len(arr.arr)); i > index; i-- {
		newArr[i] = arr.arr[i-1]
	}
	newArr[index] = item
	arr.arr = newArr
}

func (arr *singleArray[T]) Remove(index uint) T {
	result := arr.arr[index]
	for i := index + 1; i < uint(len(arr.arr)); i++ {
		arr.arr[i-1] = arr.arr[i]
	}
	arr.arr = arr.arr[:arr.Size()-1]
	return result
}

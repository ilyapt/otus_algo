package basic_structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_SingleArray(t *testing.T) {
	arr := NewSingleArray[int]()
	assert.True(t, arr.IsEmpty())
	arr.Put(4)
	assert.EqualValues(t, []int{4}, arr.arr)
	assert.EqualValues(t, 1, arr.Size())
	assert.False(t, arr.IsEmpty())
	arr.Add(1, 0)
	assert.EqualValues(t, []int{1, 4}, arr.arr)
	assert.EqualValues(t, 2, arr.Size())
	arr.Put(5)
	arr.Put(6)
	arr.Put(7)
	assert.EqualValues(t, []int{1, 4, 5, 6, 7}, arr.arr)
	assert.EqualValues(t, 5, arr.Size())
	arr.Add(2, 1)
	assert.EqualValues(t, []int{1, 2, 4, 5, 6, 7}, arr.arr)
	assert.EqualValues(t, 6, arr.Size())
	arr.Remove(3)
	assert.EqualValues(t, []int{1, 2, 4, 6, 7}, arr.arr)
	assert.EqualValues(t, 5, arr.Size())
	assert.False(t, arr.IsEmpty())
	arr.Put(9)
	assert.EqualValues(t, []int{1, 2, 4, 6, 7, 9}, arr.arr)
	assert.EqualValues(t, 6, arr.Size())
}

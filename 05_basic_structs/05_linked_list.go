package basic_structs

type listItem[T any] struct {
	value T
	next  *listItem[T]
}

type linkedList[T any] struct {
	head *listItem[T]
}

func NewLinkedList[T any]() *linkedList[T] {
	return &linkedList[T]{}
}

func (list *linkedList[T]) Put(item T) {
	if list.head == nil {
		list.head = &listItem[T]{
			value: item,
		}
		return
	}
	iter := list.head
	for iter.next != nil {
		iter = iter.next
	}
	iter.next = &listItem[T]{
		value: item,
	}
}

func (list *linkedList[T]) PutFront(item T) {
	list.head = &listItem[T]{
		value: item,
		next:  list.head,
	}
}

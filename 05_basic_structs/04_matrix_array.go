package basic_structs

type slice[T any] []T

type matrixArray[T any] struct {
	arrays    *singleArray[slice[T]]
	sliceSize uint
	len       uint
}

func NewMatrixArray[T any](sliceSize uint) *matrixArray[T] {
	return &matrixArray[T]{
		arrays:    NewSingleArray[slice[T]](),
		sliceSize: sliceSize,
	}
}

func (arr *matrixArray[T]) Size() uint {
	return arr.len
}

func (arr *matrixArray[T]) Capacity() uint {
	return arr.arrays.Size() * arr.sliceSize
}

func (arr *matrixArray[T]) Put(item T) {
	if arr.len >= arr.Capacity() {
		arr.arrays.Put(make(slice[T], arr.sliceSize))
	}
	arr.arrays.Get(arr.len / arr.sliceSize)[arr.len%arr.sliceSize] = item
	arr.len++
}

func (arr *matrixArray[T]) Get(index uint) T {
	return arr.arrays.Get(index / arr.sliceSize)[index%arr.sliceSize]
}

func (arr *matrixArray[T]) Add(item T, index uint) {
	if arr.len >= arr.Capacity() {
		arr.arrays.Put(make(slice[T], arr.sliceSize))
	}

	for i := arr.len; i > index; i-- {
		j := i - 1
		arr.arrays.Get(i / arr.sliceSize)[i%arr.sliceSize] = arr.arrays.Get(j / arr.sliceSize)[j%arr.sliceSize]
	}

	arr.arrays.Get(index / arr.sliceSize)[index%arr.sliceSize] = item
	arr.len++
}

func (arr *matrixArray[T]) Remove(index uint) T {
	result := arr.arrays.Get(index / arr.sliceSize)[index%arr.sliceSize]
	arr.len--
	for i := index; i < arr.len; i++ {
		j := i + 1
		arr.arrays.Get(i / arr.sliceSize)[i%arr.sliceSize] = arr.arrays.Get(j / arr.sliceSize)[j%arr.sliceSize]
	}
	return result
}

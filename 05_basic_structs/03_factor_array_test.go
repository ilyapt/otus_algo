package basic_structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_FactorArray(t *testing.T) {
	arr := NewFactorArray[int](5)
	arr.Set(3, 2)
	assert.EqualValues(t, []int{0, 0, 3, 0, 0}, arr.arr)
	arr.Put(4)
	assert.EqualValues(t, []int{0, 0, 3, 0, 0, 4, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 6, arr.Size())
	arr.Add(2, 2)
	assert.EqualValues(t, []int{0, 0, 2, 3, 0, 0, 4, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 7, arr.Size())
	arr.Put(5)
	arr.Put(6)
	arr.Put(7)
	arr.Put(8)
	assert.EqualValues(t, []int{0, 0, 2, 3, 0, 0, 4, 5, 6, 7, 8}, arr.arr)
	assert.EqualValues(t, 11, arr.Size())
	arr.Add(1, 1)
	assert.EqualValues(t, []int{0, 1, 0, 2, 3, 0, 0, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 12, arr.Size())
	arr.Remove(3)
	assert.EqualValues(t, []int{0, 1, 0, 3, 0, 0, 4, 5, 6, 7, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 11, arr.Size())
	arr.Put(9)
	assert.EqualValues(t, []int{0, 1, 0, 3, 0, 0, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 12, arr.Size())
}

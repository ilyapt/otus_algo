package basic_structs

const factor = 2

type factorArray[T any] struct {
	arr []T
	len uint
}

func NewFactorArray[T any](len uint) *factorArray[T] {
	return &factorArray[T]{arr: make([]T, len), len: len}
}

func (arr *factorArray[T]) Size() uint {
	return arr.len
}

func (arr *factorArray[T]) IsEmpty() bool {
	return arr.Size() == 0
}

func (arr *factorArray[T]) Capacity() uint {
	return uint(len(arr.arr))
}

func (arr *factorArray[T]) Get(index uint) T {
	if index >= arr.len {
		panic("out of range")
	}
	return arr.arr[index]
}

func (arr *factorArray[T]) Set(item T, index uint) {
	if index >= arr.len {
		panic("out of range")
	}
	arr.arr[index] = item
}

func (arr *factorArray[T]) Put(item T) {
	if arr.len >= arr.Capacity() {
		newArr := make([]T, arr.Capacity()*factor+1)
		for i := range arr.arr {
			newArr[i] = arr.arr[i]
		}
		arr.arr = newArr
	}
	arr.arr[arr.len] = item
	arr.len++
}

func (arr *factorArray[T]) Add(item T, index uint) {
	to := arr.arr // в golang это указатели
	if arr.len >= arr.Capacity() {
		newArr := make([]T, arr.Capacity()*factor+1)
		for i := uint(0); i < index; i++ {
			newArr[i] = arr.arr[i]
		}
		to = newArr
	}
	for i := arr.len; i > index; i-- {
		to[i] = arr.arr[i-1]
	}
	to[index] = item
	arr.len++
	arr.arr = to
}

func (arr *factorArray[T]) Remove(index uint) T {
	result := arr.arr[index]
	for i := index + 1; i < arr.len; i++ {
		arr.arr[i-1] = arr.arr[i]
	}
	arr.len--
	return result
}

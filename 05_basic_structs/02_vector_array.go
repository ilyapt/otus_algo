package basic_structs

type vectorArray[T any] struct {
	arr    []T
	len    uint
	vector uint
}

func NewVectorArray[T any](vector uint) *vectorArray[T] {
	return &vectorArray[T]{arr: make([]T, 0), vector: vector}
}

func (arr *vectorArray[T]) Size() uint {
	return arr.len
}

func (arr *vectorArray[T]) IsEmpty() bool {
	return arr.Size() == 0
}

func (arr *vectorArray[T]) Capacity() uint {
	return uint(len(arr.arr))
}

func (arr *vectorArray[T]) Get(index uint) T {
	if index >= arr.len {
		panic("out of range")
	}
	return arr.arr[index]
}

func (arr *vectorArray[T]) Set(item T, index uint) {
	if index >= arr.len {
		panic("out of range")
	}
	arr.arr[index] = item
}

func (arr *vectorArray[T]) Put(item T) {
	if arr.len >= arr.Capacity() {
		newArr := make([]T, arr.Capacity()+arr.vector)
		for i := range arr.arr {
			newArr[i] = arr.arr[i]
		}
		arr.arr = newArr
	}
	arr.arr[arr.len] = item
	arr.len++
}

func (arr *vectorArray[T]) Add(item T, index uint) {
	to := arr.arr // в golang это указатели
	if arr.len >= arr.Capacity() {
		newArr := make([]T, arr.Capacity()+arr.vector)
		for i := uint(0); i < index; i++ {
			newArr[i] = arr.arr[i]
		}
		to = newArr
	}
	for i := arr.len; i > index; i-- {
		to[i] = arr.arr[i-1]
	}
	to[index] = item
	arr.len++
	arr.arr = to
}

func (arr *vectorArray[T]) Remove(index uint) T {
	result := arr.arr[index]
	for i := index + 1; i < arr.len; i++ {
		arr.arr[i-1] = arr.arr[i]
	}
	arr.len--
	return result
}

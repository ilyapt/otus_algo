package basic_structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_VectorArray(t *testing.T) {
	arr := NewVectorArray[int](10)
	assert.True(t, arr.IsEmpty())
	arr.Put(1)
	assert.EqualValues(t, []int{1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	arr.Set(3, 0)
	assert.EqualValues(t, []int{3, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	arr.Put(4)
	assert.EqualValues(t, []int{3, 4, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 2, arr.Size())
	arr.Add(2, 1)
	assert.EqualValues(t, []int{3, 2, 4, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 3, arr.Size())
	arr.Put(0)
	arr.Put(0)
	arr.Put(0)
	arr.Put(5)
	arr.Put(6)
	arr.Put(7)
	arr.Put(8)
	assert.EqualValues(t, []int{3, 2, 4, 0, 0, 0, 5, 6, 7, 8}, arr.arr)
	assert.EqualValues(t, 10, arr.Size())
	arr.Add(1, 1)
	assert.EqualValues(t, []int{3, 1, 2, 4, 0, 0, 0, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 11, arr.Size())
	arr.Remove(3)
	assert.EqualValues(t, []int{3, 1, 2, 0, 0, 0, 5, 6, 7, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 10, arr.Size())
	arr.Put(9)
	assert.EqualValues(t, []int{3, 1, 2, 0, 0, 0, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arr)
	assert.EqualValues(t, 11, arr.Size())
}

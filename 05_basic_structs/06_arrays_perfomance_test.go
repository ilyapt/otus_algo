package basic_structs

import "testing"

func Test_ArraysPerformance(t *testing.T) {
	t.Run("SingleArray_10000", func(t *testing.T) {
		arr := NewSingleArray[int]()
		for i := 0; i < 10000; i++ {
			arr.Put(1)
		}
	})
	t.Run("LinkedListBack_10000", func(t *testing.T) {
		arr := NewLinkedList[int]()
		for i := 0; i < 10000; i++ {
			arr.Put(1)
		}
	})
	t.Run("SingleArray_100000", func(t *testing.T) {
		arr := NewSingleArray[int]()
		for i := 0; i < 100000; i++ {
			arr.Put(1)
		}
	})
	t.Run("Vector10Array_100000", func(t *testing.T) {
		arr := NewVectorArray[int](10)
		for i := 0; i < 100000; i++ {
			arr.Put(1)
		}
	})
	t.Run("Vector100Array_100000", func(t *testing.T) {
		arr := NewVectorArray[int](100)
		for i := 0; i < 100000; i++ {
			arr.Put(1)
		}
	})
	t.Run("Matrix10Array_100000", func(t *testing.T) {
		arr := NewMatrixArray[int](10)
		for i := 0; i < 100000; i++ {
			arr.Put(1)
		}
	})
	t.Run("Matrix100Array_100000", func(t *testing.T) {
		arr := NewMatrixArray[int](100)
		for i := 0; i < 100000; i++ {
			arr.Put(1)
		}
	})
	t.Run("LinkedListBack_100000", func(t *testing.T) {
		arr := NewLinkedList[int]()
		for i := 0; i < 100000; i++ {
			arr.Put(1)
		}
	})
	t.Run("LinkedListFront_100000", func(t *testing.T) {
		arr := NewLinkedList[int]()
		for i := 0; i < 100000; i++ {
			arr.PutFront(1)
		}
	})
	t.Run("Vector10Array_1000000", func(t *testing.T) {
		arr := NewVectorArray[int](10)
		for i := 0; i < 1000000; i++ {
			arr.Put(1)
		}
	})
	t.Run("Vector100Array_1000000", func(t *testing.T) {
		arr := NewVectorArray[int](100)
		for i := 0; i < 1000000; i++ {
			arr.Put(1)
		}
	})
	t.Run("Matrix10Array_1000000", func(t *testing.T) {
		arr := NewMatrixArray[int](10)
		for i := 0; i < 1000000; i++ {
			arr.Put(1)
		}
	})
	t.Run("Matrix100Array_1000000", func(t *testing.T) {
		arr := NewMatrixArray[int](100)
		for i := 0; i < 1000000; i++ {
			arr.Put(1)
		}
	})
	t.Run("LinkedListFront_1000000", func(t *testing.T) {
		arr := NewLinkedList[int]()
		for i := 0; i < 1000000; i++ {
			arr.PutFront(1)
		}
	})
	t.Run("FactorArray_1000000", func(t *testing.T) {
		arr := NewFactorArray[int](0)
		for i := 0; i < 1000000; i++ {
			arr.Put(1)
		}
	})
	t.Run("LinkedListFront_10000000", func(t *testing.T) {
		arr := NewLinkedList[int]()
		for i := 0; i < 10000000; i++ {
			arr.PutFront(1)
		}
	})
	t.Run("FactorArray_10000000", func(t *testing.T) {
		arr := NewFactorArray[int](0)
		for i := 0; i < 10000000; i++ {
			arr.Put(1)
		}
	})
	t.Run("LinkedListFront_100000000", func(t *testing.T) {
		arr := NewLinkedList[int]()
		for i := 0; i < 100000000; i++ {
			arr.PutFront(1)
		}
	})
	t.Run("FactorArray_100000000", func(t *testing.T) {
		arr := NewFactorArray[int](0)
		for i := 0; i < 100000000; i++ {
			arr.Put(1)
		}
	})
}

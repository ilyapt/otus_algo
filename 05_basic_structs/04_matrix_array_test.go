package basic_structs

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_MatrixArray(t *testing.T) {
	arr := NewMatrixArray[int](10)
	arr.Put(10)
	assert.EqualValues(t, 1, arr.Size())
	assert.EqualValues(t, 10, arr.Capacity())
	arr.Put(11)
	arr.Put(12)
	arr.Put(13)
	arr.Put(14)
	arr.Put(15)
	assert.EqualValues(t, 6, arr.Size())
	assert.EqualValues(t, []int{10, 11, 12, 13, 14, 15, 0, 0, 0, 0}, arr.arrays.arr[0])
	assert.EqualValues(t, 15, arr.Get(5))
	arr.Add(2, 2)
	assert.EqualValues(t, 7, arr.Size())
	assert.EqualValues(t, []int{10, 11, 2, 12, 13, 14, 15, 0, 0, 0}, arr.arrays.arr[0])
	arr.Put(16)
	arr.Put(17)
	arr.Put(18)
	assert.EqualValues(t, 10, arr.Size())
	assert.EqualValues(t, 10, arr.Capacity())
	assert.EqualValues(t, []int{10, 11, 2, 12, 13, 14, 15, 16, 17, 18}, arr.arrays.arr[0])
	arr.Add(3, 3)
	assert.EqualValues(t, 11, arr.Size())
	assert.EqualValues(t, 20, arr.Capacity())
	assert.EqualValues(t, []int{10, 11, 2, 3, 12, 13, 14, 15, 16, 17}, arr.arrays.arr[0])
	assert.EqualValues(t, []int{18, 0, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arrays.arr[1])
	arr.Add(5, 5)
	assert.EqualValues(t, 12, arr.Size())
	assert.EqualValues(t, 20, arr.Capacity())
	assert.EqualValues(t, []int{10, 11, 2, 3, 12, 5, 13, 14, 15, 16}, arr.arrays.arr[0])
	assert.EqualValues(t, []int{17, 18, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arrays.arr[1])
	arr.Remove(4)
	assert.EqualValues(t, 11, arr.Size())
	assert.EqualValues(t, 20, arr.Capacity())
	assert.EqualValues(t, []int{10, 11, 2, 3, 5, 13, 14, 15, 16, 17}, arr.arrays.arr[0])
	assert.EqualValues(t, []int{18, 18, 0, 0, 0, 0, 0, 0, 0, 0}, arr.arrays.arr[1])
}

package lucky_ticket

func LuckyTicketN(N uint) uint64 {
	var arr []uint64
	for i := 0; i < int(N); i++ {
		arr = calc(arr)
	}

	var cnt uint64 = 0
	for _, val := range arr {
		cnt = cnt + (val * val)
	}
	return cnt
}

func calc(in []uint64) []uint64 {
	if len(in) == 0 {
		in = []uint64{1}
	}

	out := make([]uint64, len(in)+9)
	for i := 0; i < 10; i++ {
		for j := 0; j < len(in); j++ {
			out[i+j] = out[i+j] + in[j]
		}
	}
	return out
}

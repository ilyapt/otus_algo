package lucky_ticket

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"strconv"
	"strings"
	"testing"
)

const filesPath = "./1.Tickets/"

func Test_LuckyTickets(t *testing.T) {
	for i := 0; i < 10; i++ {
		inData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.in", filesPath, i))
		assert.NoError(t, err)
		in, err := strconv.Atoi(strings.TrimSpace(string(inData)))
		assert.NoError(t, err)
		outData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.out", filesPath, i))
		assert.NoError(t, err)
		out, err := strconv.ParseUint(strings.TrimSpace(string(outData)), 10, 64)
		assert.NoError(t, err)

		t.Run(fmt.Sprintf("test_for_%d_digits", in), func(t *testing.T) {
			result := LuckyTicketN(uint(in))
			assert.Equal(t, out, result)
			fmt.Printf("\tresult: %d\n", result)
		})
	}
}

package lucky_ticket

func LuckyTicketN(N uint) uint64 {
	arr := make([]uint64, 9*N+1)
	calc(arr, N, 0)

	var cnt uint64 = 0
	for _, val := range arr {
		cnt = cnt + (val * val)
	}
	return cnt
}

func calc(q []uint64, N uint, sum int) { // массив всегда передается по ссылке, поэтому ничего не возвращаем, просто пишем в него
	if N == 0 {
		q[sum]++
		return
	}
	for i := 0; i <= 9; i++ {
		calc(q, N-1, sum+i)
	}
}

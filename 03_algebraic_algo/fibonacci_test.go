package algebraic_algo

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"strconv"
	"strings"
	"testing"
)

const fibonacciFilesPath = "./4.Fibo/"

func Test_Fibonacci(t *testing.T) {
	for i := 0; i < 13; i++ {
		inData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.in", fibonacciFilesPath, i))
		require.NoError(t, err)
		in, err := strconv.Atoi(strings.TrimSpace(string(inData)))
		require.NoError(t, err)
		outData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.out", fibonacciFilesPath, i))
		require.NoError(t, err)
		out := strings.TrimSpace(string(outData))

		t.Run(fmt.Sprintf("рекурсивный алгоритм поиска чисел Фибоначчи %d", i), func(t *testing.T) {
			if i > 6 {
				t.Skip("слишком долго чтобы ждать в тестах")
			}
			res := recursiveFib(uint(in))
			assert.Equal(t, out, res.String())
		})

		t.Run(fmt.Sprintf("итеративный алгоритм поиска чисел Фибоначчи %d", i), func(t *testing.T) {
			if i > 11 {
				t.Skip("слишком долго чтобы ждать в тестах")
			}
			res := iterativeFib(uint(in))
			assert.Equal(t, out, res.String())
		})

		t.Run(fmt.Sprintf("поиск чисел Фибоначчи по формуле золотого сечения %d", i), func(t *testing.T) {
			if i > 6 {
				t.Skip("некорректный рассчет из-за накопления ошибки в float64")
			}
			r := goldenRatioFib(uint(in))
			assert.Equal(t, out, r.String())
		})

		t.Run(fmt.Sprintf("поиск чисел Фибоначчи через умножение матриц %d", i), func(t *testing.T) {
			r := matrixFib(uint(in))
			assert.Equal(t, out, r.String())
		})
	}
}

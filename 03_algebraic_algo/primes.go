package algebraic_algo

import (
	"math"
)

func divisorEnumerationPrimes(n int) (cnt uint) {
	for i := 2; i <= n; i++ {
		if isPrime1(i) {
			cnt++
		}
	}
	return cnt
}

func isPrime1(n int) bool {
	m := int(math.Sqrt(float64(n)))
	for i := 2; i <= m; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}

var primes []int

func divisorPrimeEnumerationPrimes(n int) (cnt uint) {
	for i := 2; i <= n; i++ {
		if isPrime2(i) {
			primes = append(primes, i)
			cnt++
		}
	}
	return cnt
}

func isPrime2(n int) bool {
	if n == 2 {
		return true
	}
	l := len(primes)
	m := int(math.Sqrt(float64(n)))
	for i := 0; i < l && primes[i] <= m; i++ {
		if n%primes[i] == 0 {
			return false
		}
	}
	return true
}

func simpleEratosthenesPrimes(n int) (cnt uint) {
	lp := make([]bool, n+1)
	for i := 2; i <= n; i++ {
		if !lp[i] {
			cnt++
		}
		for j := i * i; j <= n; j += i {
			lp[j] = true
		}
	}
	return cnt
}

func onlyOddEratosthenesPrimes(n int) (cnt uint) {
	if n < 3 {
		return uint(n) - 1
	}
	lp := make([]bool, n/2)
	for i := 3; i <= n; i += 2 {
		if !lp[i/2-1] {
			cnt++
		}
		for j := i * i; j <= n; j += i {
			if j%2 == 1 {
				lp[j/2-1] = true
			}
		}
	}
	return cnt + 1
}

func timeOptimizeEratosthenesPrimes(n int) uint {
	lp := make([]int, n+1)
	var primes []int
	for i := 2; i <= n; i++ {
		if lp[i] == 0 {
			lp[i] = i
			primes = append(primes, i)
		}

		l := len(primes)
		for j := 0; j < l && primes[j] <= lp[i] && primes[j]*i <= n; j++ {
			lp[primes[j]*i] = primes[j]
		}
	}
	return uint(len(primes))
}

package algebraic_algo

func iterativePow(base float64, exp int) float64 {
	res := float64(1)
	for i := 0; i < exp; i++ {
		res = res * base
	}
	return res
}

func multiplicationPow(base float64, exp int) float64 {
	if exp == 0 {
		return 1
	}
	if exp == 1 {
		return base
	}

	p := base

	var i int
	for i = 1; i*2 <= exp; i *= 2 {
		p = p * p
	}

	for i < exp {
		p = p * base
		i++
	}
	return p
}

func binaryDecompositionPow(base float64, exp int) float64 {
	var p float64 = 1

	if exp == 0 {
		return p
	}

	for exp > 1 {
		if exp%2 == 1 {
			p *= base
		}
		
		base *= base
		exp /= 2
	}

	return p * base
}

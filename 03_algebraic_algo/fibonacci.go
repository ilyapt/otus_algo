package algebraic_algo

import (
	"fmt"
	"math"
	"math/big"
)

func recursiveFib(n uint) *big.Int {
	if n == 0 {
		return big.NewInt(0)
	}
	if n <= 2 {
		return big.NewInt(1)
	}
	return new(big.Int).Add(recursiveFib(n-1), recursiveFib(n-2))
}

func iterativeFib(n uint) *big.Int {
	if n == 0 {
		return big.NewInt(0)
	}
	if n <= 2 {
		return big.NewInt(1)
	}
	a := big.NewInt(1)
	b := big.NewInt(1)
	var i uint
	for i = 2; i < n; i++ {
		tmp := new(big.Int).Add(a, b)
		a = b
		b = tmp
	}
	return b
}

func goldenRatioFib(n uint) *big.Int {
	e := math.Sqrt(5)
	f := (1 + e) / 2

	r, _ := new(big.Float).Add(new(big.Float).Quo(bigPow(big.NewFloat(f), n), big.NewFloat(e)), big.NewFloat(0.5)).Int(nil)
	return r
}

func bigPow(base *big.Float, exp uint) *big.Float {
	m := new(big.Float).Set(base)
	p := new(big.Float).Set(base)

	if exp == 0 {
		return big.NewFloat(1)
	}
	if exp == 1 {
		return m
	}

	exp--
	for exp > 1 {
		if exp%2 == 1 {
			p = new(big.Float).Mul(p, m) //p.mul(m)
		}

		m = new(big.Float).Mul(m, m) //m.mul(m)
		exp /= 2
	}

	return new(big.Float).Mul(p, m) //p.mul(m)
}

type matrix [2][2]*big.Int

func (m matrix) mul(n matrix) matrix {
	return matrix{
		{new(big.Int).Add(new(big.Int).Mul(m[0][0], n[0][0]), new(big.Int).Mul(m[0][1], n[1][0])),
			new(big.Int).Add(new(big.Int).Mul(m[0][0], n[0][1]), new(big.Int).Mul(m[0][1], n[1][1]))},
		{new(big.Int).Add(new(big.Int).Mul(m[1][0], n[0][0]), new(big.Int).Mul(m[1][1], n[1][0])),
			new(big.Int).Add(new(big.Int).Mul(m[1][0], n[0][1]), new(big.Int).Mul(m[1][1], n[1][1]))},
	}
}

func (m matrix) sqrt() matrix {
	return m.mul(m)
}

func (m matrix) print() {
	fmt.Printf("%s\t%s\n%s\t%s\n", m[0][0].String(), m[0][1].String(), m[1][0].String(), m[1][1].String())
}

func (m matrix) pow(exp uint) matrix {
	p := m

	if exp == 0 {
		return matrix{}
	}
	if exp == 1 {
		return m
	}

	exp--
	for exp > 1 {
		if exp%2 == 1 {
			p = p.mul(m)
		}

		m = m.mul(m)
		exp /= 2
	}

	return p.mul(m)
}

func matrixFib(n uint) *big.Int {
	if n < 2 {
		return big.NewInt(int64(n))
	}
	m := matrix{{big.NewInt(1), big.NewInt(1)}, {big.NewInt(1), big.NewInt(0)}}
	return m.pow(n - 1)[0][0]
}

package algebraic_algo

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"strconv"
	"strings"
	"testing"
)

const primeFilesPath = "./5.Primes/"

func Test_Primes(t *testing.T) {
	for i := 0; i < 15; i++ {
		inData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.in", primeFilesPath, i))
		require.NoError(t, err)
		in, err := strconv.Atoi(strings.TrimSpace(string(inData)))
		require.NoError(t, err)
		outData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.out", primeFilesPath, i))
		require.NoError(t, err)
		out, err := strconv.ParseUint(strings.TrimSpace(string(outData)), 10, 32)
		require.NoError(t, err)

		t.Run(fmt.Sprintf("поиск количества простых чисел через перебор делителей %d", i), func(t *testing.T) {
			if i == 13 {
				t.Skip("слишком долго чтобы ждать в тестах")
			}
			r := divisorEnumerationPrimes(in)
			assert.Equal(t, uint(out), r)
		})
		t.Run(fmt.Sprintf("поиск количества простых чисел делением только на простые числа %d", i), func(t *testing.T) {
			if i == 13 {
				t.Skip("слишком долго чтобы ждать в тестах")
			}
			primes = nil
			r := divisorPrimeEnumerationPrimes(in)
			assert.Equal(t, uint(out), r, fmt.Sprintf("%d - %d:%d", in, out, r))
		})
		t.Run(fmt.Sprintf("поиск количества простых чисел решетом Эратосфена %d", i), func(t *testing.T) {
			r := simpleEratosthenesPrimes(in)
			assert.Equal(t, uint(out), r, fmt.Sprintf("%d - %d:%d", in, out, r))
		})
		t.Run(fmt.Sprintf("решето Эратосфена только с нечетными числами %d", i), func(t *testing.T) {
			r := onlyOddEratosthenesPrimes(in)
			assert.Equal(t, uint(out), r, fmt.Sprintf("%d - %d:%d", in, out, r))
		})
		t.Run(fmt.Sprintf("решето Эратосфена за время O(n) %d", i), func(t *testing.T) {
			if i == 13 {
				t.Skip("слишком долго чтобы ждать в тестах")
			}
			r := timeOptimizeEratosthenesPrimes(in)
			assert.Equal(t, uint(out), r, fmt.Sprintf("%d - %d:%d", in, out, r))
		})

	}
}

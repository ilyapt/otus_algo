package algebraic_algo

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"strconv"
	"strings"
	"testing"
)

const powerFilesPath = "./3.Power/"

func Test_Power(t *testing.T) {
	for i := 0; i < 10; i++ {
		inData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.in", powerFilesPath, i))
		require.NoError(t, err)
		parts := bytes.Split(inData, []byte("\n"))
		base, err := strconv.ParseFloat(strings.TrimSpace(string(parts[0])), 64)
		require.NoError(t, err)
		exp, err := strconv.ParseUint(strings.TrimSpace(string(parts[1])), 10, 64)
		require.NoError(t, err)
		outData, err := ioutil.ReadFile(fmt.Sprintf("%s/test.%d.out", powerFilesPath, i))
		require.NoError(t, err)
		out, err := strconv.ParseFloat(strings.TrimSpace(string(outData)), 64)
		require.NoError(t, err)

		t.Run(fmt.Sprintf("степень_итерациями_%d", i), func(t *testing.T) {
			res := iterativePow(base, int(exp))
			assert.Equal(t, fmt.Sprintf("%.11f", out), fmt.Sprintf("%.11f", res))
		})

		t.Run(fmt.Sprintf("степень через домножение_%d", i), func(t *testing.T) {
			res := multiplicationPow(base, int(exp))
			assert.Equal(t, fmt.Sprintf("%.6f", out), fmt.Sprintf("%.6f", res))
		})

		t.Run(fmt.Sprintf("степень через двоичное разложение_%d", i), func(t *testing.T) {
			res := binaryDecompositionPow(base, int(exp))
			assert.Equal(t, fmt.Sprintf("%.6f", out), fmt.Sprintf("%.6f", res))
		})
	}
}

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

uint64_t onlyOddEratosthenesPrimes(uint64_t n) {
	if (n < 3) {
		return (uint64_t)n - 1;
	}

    char *lp;
    lp = (char *)malloc(sizeof(char) * (n/2+1));

	for (uint64_t i = 0; i < n/2; i++ ) {
	    lp[i] = 0;
	}

	uint64_t cnt = 0;
	uint64_t ops_cnt = 0;

    for (uint64_t i = 3; i <= n; i += 2) {
    		if (lp[i/2-1] == 0) {
    			cnt++;
    			ops_cnt++;
    		}
    		for (uint64_t j = i * i; j <= n; j += i) {
    		    ops_cnt++;
    			if (j%2 == 1) {
    				lp[j/2-1] = 1;
    			}
    		}
    	}

    printf("ops %llu\n", ops_cnt);
    return cnt+1;
}

uint64_t timeOptimizeEratosthenesPrimes(uint64_t n) {
    uint64_t *lp;
    lp = (uint64_t *)malloc(sizeof(uint64_t) * (n+1));
	for (uint64_t i = 0; i <= n; i++ ) {
	    lp[i] = 0;
	}

    uint64_t *primes;
    primes = (uint64_t *)malloc(sizeof(uint64_t) * (50847534));
    uint64_t l = 0;
    uint64_t ops_cnt = 0;

    for (uint64_t i = 2; i <= n; i++) {
    	if (lp[i] == 0) {
    		lp[i] = i;
    		primes[l++] = i;
    		ops_cnt++;
    	}

    	for (uint64_t j = 0; j < l && primes[j] <= lp[i] && primes[j] * i <= n; j++) {
    	    lp[primes[j] * i] = primes[j];
    	    ops_cnt++;
    	}
    }

    printf("ops %llu\n", ops_cnt);
    return l;
}

int main() {
    uint64_t x = timeOptimizeEratosthenesPrimes(1000000000);
    printf("primes %llu\n", x);
    return 0;
}

